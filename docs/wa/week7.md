#07.

##  THE WAY THINGS WORK

##LOG OF THE WEEK  
- 12/11   :  Intro to Arduino
             and sensor exploration
- 13/11     :  Introduction to Communication Protocols and sensor exploration  on Arduino
- 14/11     :  Project Development on Arduino

- 15/11     : NodeRed
- 16/11     :  Project Development

##REFLECTIONS :

- This week was super fun with Guilliam and Victor . This week started with introduction to computing and networks . This was the introduction for physical computing using Arduino .
The first thing that we had to do was we had to disassemble various electronic equipment and study its insides . In this process we learnt various equipment that goes inside the everyday electronics equipment . Our group decided to open a telephone . We expected the telephone to be quite analog in nature and not super  complicated but as it turned out the telephone was of a corporate nature . It consisted of various digital circuitry to encode and decode the messages passing through the EPBAX system . We read various datasheets of the chips that we found on the board including various discrete components .Some of the datasheets of the Chips could not be found because the they were either obsolete or the companies had shutdown .
One of the most interesting things to see after seeing the breakdown of various printers was various strategies and methodologies used in design of the same component . Various cost cutting component in the use of the telephone was the intergration of the switches on the PCB of the telephone itself rather that having them discrete.

- We had to create a installations for the classroom which were to based on a singular input and output that meant that we had to either choose an input or an output .My group decided to explore the  various options for a day and then decided to play around with various sensors which included temp sensor , audio sensor (mic ) , ultrasonic sensor etc .
During the exploring phase of the first day we were allowed to explore Arduino on our own. I decided to use it as a starter to explore capacitive sensing.
I just connected the Analog sensor which in this case is a wire coiled around like an antenna. This was the only done one ADC, and basic analog read serial in the examples.
1![](w7-door.jpg)
![](test.png)
We wanted to measure the “mood” of the room i.e excited, relaxed, tired this would be calculated using sensors such as the ambient light in the room, the ambient noise of the room and also the number of people in the room.
We encountered various problems during using an Ultrasonic sensors, key tip that I noticed when using the ultrasonic sensor is that the reflected surface always needs to perpendicular to the transmitter and the receiver .
This was implemented using 2 ESP8266 board. One of the ESP8266 board was used to measure the noise and light of the room. The other ESP8266 was used to count the number of people in the room.
![](w7-light.jpg)
- This was ported to later on ESP8266 which was the most important discovery of the week .This has been one of the best modules that I have worked on Victor and Guilliam talked about various communication protocols and methodologies .  The step of setting up a RaspberryPi  was super simple with a program called Mosquito which can handle MQTPP protocol for communication .
![](w7-datacomm.jpg)
