#08.

##  BE YOUR OWN SYSTEM (LIVING WITH IDEAS )

##LOG OF THE WEEK  
- 19/11     :  Counter Artefacts ( Toothbrush Experiment )
- 20/11     :  Living with Ideas (Actual Realities)
- 21/11     :  Project Development on Arduino
- 22/11     :  
- 23/11     :  

##REFLECTIONS :

- "This was the living with ideas week . Which was the living with your prototype , was to bring something from our own .and to be used by someone else. Thus went out the idea of having a prototype of existing things .

- The experiment was called as using a “counter artefact”- This is the process of using an everyday item in an unusual way.  
I used a toothbrush to make a “wearable sensor” that had to be broken to be used or deployed . I was thinking about the context of the user .  Oscar suggested I find the place where the firstaid kit is put . The process of finding and the first aid kit I talked to Santi , he expressed intrest in the project that he would and use cases of the wearable in action.
My experience on the actual experience was totally different than that  I was envisioning . The whole process of “deployment” of the the sensor . It gave me insights into what was the entire process from finding it from the first aid kit box to actually applying it .

- Following are the pictures from that exercise
 ![](week8-firstaid.jpg)
- Being your own system where we used a green screen app to experience realities for ourselves.
- Next we had to make our own magic systems from 16 key emotions. I choose the word “Tranquillity”, which most resonated with me. I took the usual root for tranquillity of Japanese rock gardens and converting it into a low resolution cardboard rendering.  But after chat with David he suggested that this exercise was about making machines. So I started thinking about what make somebody “  tranquil “ It’s the state of which involves visual , aural stimulation and also orafactory stimulation. That means It needs to have video , audio and aromas . So I decided to make a helmet ,with these inbuilt capabilities.
It called it the tranqilator 9000 , ironically .
![](week8-t1.jpg)
This was the exterior of the helmet ,this was made from unfolding the bag

![](week8-t2.jpg)
This was the inside the helmet , the big rectangle indicates the screen for the visual stimulation.
The plastic spoons indicate the orafactory stimulators i.e aroma which will be used to enhance the entire experience .

### Reflections of this week :
This was quite a journey for me, because I started this week questioning the need for speculative design and considered them a bit out there. As I progressed through I understood and appreciated its place in the practice of design.  It’s like pulling a string of thought from the nebulous void of our own consciousness.


Speculative design is illustrated in the following way as I understand it .
![](week8-spec.jpg)
The red lines denote the nebulous consciousness and the black line is the the thought of the project or an idea .
