#02. BIOLOGY ZERO

##LOG OF THE WEEK  
- OCT 8: -Overview of BIOLOGY .
        - Evolution of Biology .
        - Cultural Consciousness.
        - Phages / CRISPR-Cas9
        - "Future is here but not everywhere- William Gibson  "
        - IgEm
        - Mette Andersen : Material Design Lab
        - Bee Silk - unconventional Material
        - Scientific Method
      Lab 1 : Sterility and 3 different mediums and cultures .
        - Jonathan discussed possibility of sampling and understanding using the Scientific method .
- OCT 9 : -  Nuria : Introduction to DIY BIOLOGY.
          -  History of BIOLOGY.
          -  Reason for BIOLOGY -
                                - Reducing the price
                                - Personalization of the machinery.

          - Introduction to laboratory equipment .
          - Biochemistry
          - Machines - Spectrophotometer
                          - Transmittance and Absorbance
                          - Beer's Law
      Lab 2 : Measuring colourant in a Solution  
          - Measuring the various colours i.e Red, Blue , Yellow Dyes .
          - Laser was red laser so the colour were not correct.
          -                                             

- OCT 10:  - Consilience
           - Metabolism , Microbiology , Photosynthesis
           - How to make the cultures i.e stricking
           classification of various organisms.
           - humanmicrobiome project.
      Lab 3 : Spirulina and Microscopes .
           - Recipe for growing Spirulina .

- OCT 11:  - Social Agriculture
           - Translation and Transcription
      Lab 4 : PCR Machine  
            - DNA Amplification Machine

------------------------------------------------------
###  What did you do that is new. What did you learn?
-
- Day1 : Learnt various aspects of biology from the very minute to the complex systems which are involved . Learnt from Mette about the various new materials which are being used . most notably Bee-silk .
Lab1 : auxitic medium , existence of bioluminescent bacteria in squids .
- Day 2 : Learnt about the DIY - BIO scene and its origins. How biology orginated from the republic of letters to the current climate .To the engineering aspect of people building their own machines to get around exorbitant prices quoted by manufacturers and also to personalize each machine to their specifications .Learnt about various lab equipment which are commonly used . This was also the time Nuria explained various DIY alternatives to lab equipment . These equipment use a basic physics principle which is used to extract data , sometimes the sample needs to be treated before putting it into the machine.
Lab2 : Learning about the cultures that we made . Did 2 experiments i.e. 1 . Finding the amount of colorant in a Solution . 2. Finding the amount of Calcium in the eggshells.
- Day 3 : Various things about lipid membranes , Amino Acids , Nucleotides ,
- Day 4 : All things about DNA .
    Lab 4  : DNA Amplification in the PCR machine . and using the electrophoresis machine .
- Day 5  : Learning the scale of biomedical research happening in Barcelona


### How is this relevant to your work?
- The Experiment :  Capacitive Sensing and regulation for medical devices using bio-chemical agents . This would possibility lead to a skin tattoo which will be applied before the device is put on the skin .

Hypothesis : Non-Invasive biological Capacitive sensing can provide regulation , buffering , caliberation and control for on-skin devices .


Methodology :
   1. Studying various bacteria which produce a certain secretion ,
   2. Setting up and testing a test bench for Measuring the electrical deviation .
   3. Test various cultures and ranges .
   4. Repeat until the readings have stabilized .

Possible Outcomes : Better and low cost probes for on-skin medical devices.
------------------------------------------------------
### Reading list for this week -

  1. Cyborg Manifesto
  2. https://thelabrat.com
  3. Butterflies of the soul
  4. https://diybio.org/local
  5. Consilience
  6. Ron Eglash - Fractals of Africa.
------------------------------------------------------

### Personal Reflection points and thoughts of the week  
 1. BIOLOGY is lot more interesting than i thought it would be
 2. Wearables and Biology is an interesting space to explore .  
