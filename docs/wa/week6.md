#06
##  Navigating Uncertainity

##LOG OF THE WEEK  
- 5/11   :  Intro to Extended Intelligence.
	          Allegory of the cave and the symbol grounding problem
	          Darthmouth conference
- 6/11     :  Starting a sustainability agent
	          Intelligent Agent
- 7/11     :  Machine Learning
	          Sklearn(python)
	          Mklearn
	          BigML
- 8/11     :  BigML
	          Wekinator
- 9/11     :  Ethics of A.I

##REFLECTIONS :
- It started with discussions of intelligence. Theories of Plato and Socrates. We first defined what an intelligent agent is
----------------------------------------------------------------------------------------------------------------
- “Intelligent agent was i.e. an agent which can “respond to stimuli from the environment and adapt to it in order to achieve is goal”.
----------------------------------------------------------------------------------------------------------------
- Our group decided to design an intelligent agent that would make the consumers in making sustainable choices. From choosing groceries to choosing the electronics goods which will ultimately better choice for the environment.
<iframe width="560" height="315" src="https://www.youtube.com/embed/lVDaSgyi3xE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### DAY 1 OF DESIGNING AN AGENT
- Goals:  Assist in making sustainable decisions.
- Contents: Before buying it / when you are on the store online and offline
- Actors/Agents:  
                1. Manufacturer agent
	             	2. Trustable / Quality Service (who checks)
	            	3. Consumer Agent (APP)
- Types of tasks:
                1.	Building a database
                2.	Detect a product
                3.	Compare the products (testing – destructive testing of the products )

- Inputs: Material / Product

- Process:  Manufacture
	          Chemicals used
	          Conditions of workers
	          Recycling level
	          CO2 emissions
	          Transport Footprint

- Output:   Rating System for showing in app/wearables/website



### DAY 2 OF DESIGNING AN AGENT
- We talked about basics of data organization shown in the photo below.
The discussion mostly entered of clustering of datasets within the entire dataset of the agent that is organic objects / inorganic objects. Etc.
![](w6-dataset.jpg)


###DAY 3 OF DESIGNING AN AGENT
- On this day we broke down the processes that will go in process of identifying the product to assigning a score on the app. below photograph shows the block diagram and the flowchart of the data and the process.
![](w6-aiprocess.jpg)


### DAY 4 OF DESIGNING AN AGENT

- The designed agent was analyzed according to Safety, Fairness of the agent.
	Ethics of the sustainability agent
1.	Certification committee 	
a.	CO2 output of the producing the product
b.	Overall industry of the CO2 output
2.	Destructive Testing of products (Rechecking the data provided by the manufacturers.)
3.	Judging authentic reviews of customers on the app.
4.	Evaluating the energy consumption of A.I (sustainability agent )
